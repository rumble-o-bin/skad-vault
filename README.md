# skad-vault

```bash
File .vault.txt should be add to .gitignore files
Remember its just PoC only, because sometimes i forgot how use this 
```

### Simple steps

- encrypt

`ansible-vault encrypt roles/skad/vars/enc.yml --vault-password-file .vault.txt`

- run a play!

`ansible-playbook -i inventory.yml play-skad.yml --vault-password-file=.vault.txt`